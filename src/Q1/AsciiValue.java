package Q1;

import java.util.Scanner;

public class AsciiValue {

	public static void main(String[] args) {
		Scanner ch = new Scanner(System.in);
		char cha = ch.next().charAt(0);
		// variable that stores the integer value of the character  
		int asciivalue = cha;  
	  
		System.out.println("The ASCII value of " + cha + " is: " + asciivalue);  
		
		

	}

}
